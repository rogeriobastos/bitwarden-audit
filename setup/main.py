import json
from elasticsearch import Elasticsearch, NotFoundError
from elasticsearch.helpers import bulk
from os import environ as env

from device import device_type
from event import event_type


def es_client():
    return Elasticsearch(
        hosts=env['ES_URL'],
        verify_certs=False,
        api_key=env['ES_APIKEY']
    )


def gendata(data):
    for key, value in data.items():
        d = {
            'id': key,
            'description': value,
        }
        yield d


def main():
    es = es_client()

    # Create index templates
    templates = ['bitwarden-users', 'logs-bitwarden.audit']
    for template in templates:
        if not es.indices.exists_index_template(name=template):
            with open(f'index-template/{template}.json') as f:
                j = json.load(f)
            print(f'Creating index template {template}')
            es.indices.put_index_template(name=template, index_patterns=j['index_patterns'], priority=j['priority'], template=j['template'], data_stream=j.get('data_stream', None))

    # Create indexes
    indices = ['bitwarden-devices', 'bitwarden-events', 'bitwarden-users']
    for index in indices:
        if not es.indices.exists(index=index):
            print(f'Creating index {index}')
            es.indices.create(index=index)
    
    bulk(client=es, actions=gendata(device_type), index='bitwarden-devices', refresh='wait_for')
    bulk(client=es, actions=gendata(event_type), index='bitwarden-events', refresh='wait_for')

    # Create enrich policies
    policies = [
        'bitwarden-device',
        'bitwarden-event',
        'bitwarden-member',
        'bitwarden-user'
    ]
    for policy in policies:
        p = es.enrich.get_policy(name=policy)
        if len(p['policies']) == 0:
            with open(f'enrich-policy/{policy}.json') as f:
                j = json.load(f)
            print(f'Creating enrich policy {policy}')
            es.enrich.put_policy(name=policy, match=j['match'])
            es.enrich.execute_policy(name=policy)

    # Create pipeline
    pipeline = 'logs-bitwarden.audit'
    try:
        es.ingest.get_pipeline(id=pipeline)
    except NotFoundError:
        with open('pipeline/default.json') as f:
            j = json.load(f)
        print(f'Creating pipeline {pipeline}')
        es.ingest.put_pipeline(id=pipeline, processors=j['processors'])

    # Create data stream
    data_stream = 'logs-bitwarden.audit'
    print(f'Creating data stream {data_stream}')
    es.indices.create_data_stream(name=data_stream)

    # TODO: Create API key with least privilege

if __name__ == "__main__":
    main()
