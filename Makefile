APP_NAME   := bitwarden-audit
GIT_COMMIT := $(shell git rev-parse --short HEAD)

.PHONY: docker-build
docker-build:
	docker build -t ${APP_NAME}:latest .

.PHONY: docker-push
docker-push:
	docker tag ${APP_NAME}:latest ${DOCKER_REGISTRY}${APP_NAME}:latest
	docker tag ${APP_NAME}:latest ${DOCKER_REGISTRY}${APP_NAME}:${GIT_COMMIT}
	docker push ${DOCKER_REGISTRY}${APP_NAME}:latest
	docker push ${DOCKER_REGISTRY}${APP_NAME}:${GIT_COMMIT}
