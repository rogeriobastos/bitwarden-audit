from elasticsearch import Elasticsearch
from os import environ as env
from requests import Session
from requests.adapters import HTTPAdapter


def http_client():
    s = Session()
    s.mount('http://', HTTPAdapter(max_retries=3))
    s.mount('https://', HTTPAdapter(max_retries=3))
    return s


def es_client():
    return Elasticsearch(
        hosts=env['ES_URL'],
        verify_certs=False,
        api_key=env['ES_APIKEY']
    )


def bw_get_token():
    client_id = env['BW_CLIENT_ID']
    client_secret = env['BW_CLIENT_SECRET']

    d = {
            "grant_type": "client_credentials",
            "scope": "api.organization",
            "client_id": client_id,
            "client_secret": client_secret
    }
    http_session = http_client()
    r = http_session.post("https://identity.bitwarden.com/connect/token", data=d, timeout=10)
    return r.json()['access_token']
