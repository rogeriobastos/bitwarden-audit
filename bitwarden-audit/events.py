from datetime import datetime, timedelta, timezone
from elasticsearch.helpers import bulk
from time import sleep
from utils import http_client


def es_get_last_event(es, index):
    r = es.search(index=index, size=1, sort='@timestamp:desc', query={"match_all": {}})
    if len(r.body['hits']['hits']) == 0:
        return None
    tstamp = r.body['hits']['hits'][0]['_source']['@timestamp']
    return datetime.strptime(tstamp, '%Y-%m-%dT%H:%M:%S.%fZ').replace(tzinfo=timezone.utc)


def calc_tstamp_start(es, index):
    tstamp_last_event = es_get_last_event(es, index)
    if tstamp_last_event is None:
        return datetime.utcnow() - timedelta(days=30)
    return tstamp_last_event.replace(microsecond=0) + timedelta(seconds=1)


def bw_get_events_page(token, start, end, continuation=None):
    url = "https://api.bitwarden.com/public/events"
    h = {
            "Authorization": f"Bearer {token}",
            "Content-type": "application/json"
    }
    p = {
            "start": start,
            "end": end,
            "continuationToken": continuation
    }
    http_session = http_client()
    r = http_session.get(url, headers=h, params=p, timeout=10)
    return r.json()


def bw_get_events(token, start, end):
    r = bw_get_events_page(token, start, end)
    d = r['data']
    c = r['continuationToken']
    while c is not None:
        sleep(2)
        r = bw_get_events_page(token, start, end, c)
        d.extend(r['data'])
        c = r['continuationToken']
    d.reverse()
    return d


def es_gendata(data, bw_token):
    for d in data:
        doc = {
            '_op_type': 'create',
            '_source': {
                'bitwarden': d,
                'data_stream': {
                    'dataset': 'bitwarden.audit',
                    'type': 'logs'
                }
            }
        }
        yield doc


def collect_events(es, es_index, bw_token):
    tstamp_end = datetime.utcnow() - timedelta(minutes=5)
    tstamp_start = calc_tstamp_start(es, es_index)
    d = bw_get_events(bw_token, tstamp_start.isoformat(timespec='seconds'), tstamp_end.isoformat(timespec='seconds'))
    bulk(client=es, actions=es_gendata(d, bw_token), index=es_index, refresh='wait_for')
