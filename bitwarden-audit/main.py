import argparse

from utils import bw_get_token, es_client
from events import collect_events
from users import update_user_index



def main():
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-e', action='store_true', help='Collect audit events')
    group.add_argument('-u', action='store_true', help='Update user index')
    args = parser.parse_args()

    es = es_client()
    bw_token = bw_get_token()

    if args.e:
        collect_events(es, 'logs-bitwarden.audit', bw_token)
    if args.u:
        update_user_index(es, 'bitwarden-users', bw_token)


if __name__ == "__main__":
    main()
