from elasticsearch.helpers import bulk
from time import sleep
from utils import http_client


def bw_get_members_page(token, continuation=None):
    url = "https://api.bitwarden.com/public/members"
    h = {
            "Authorization": f"Bearer {token}",
            "Content-type": "application/json"
    }
    p = {"continuationToken": continuation} if continuation else None
    http_session = http_client()
    r = http_session.get(url, headers=h, params=p, timeout=10)
    return r.json()


def bw_get_members(bw_token):
    r = bw_get_members_page(bw_token)
    d = r['data']
    c = r['continuationToken']
    while c is not None:
        sleep(2)
        r = bw_get_members_page(bw_token, c)
        d.extend(r['data'])
        c = r['continuationToken']
    return d


def es_gendata(data):
    for d in data:
        doc = {
            '_op_type': 'create',
            '_source': {
                'id': d['id'],          # unique identifier within the organization
                'email': d['email'],
                'userId': d['userId'],  # unique identifier across Bitwarden (only confirmed accounts has it)
            }
        }
        yield doc


def update_user_index(es, es_index, bw_token):
    d = bw_get_members(bw_token)
    es.delete_by_query(index=es_index, query={"match_all": {}})   # delete all docs
    bulk(client=es, actions=es_gendata(d), index=es_index, refresh='wait_for') # insert everything again
    es.enrich.execute_policy(name='bitwarden-user')               # update enrich index
    es.enrich.execute_policy(name='bitwarden-member')             # update enrich index
