FROM alpine:3.18
RUN adduser --uid 1000 --ingroup nogroup --shell /usr/sbin/nologin --home /nonexistent --no-create-home --disabled-password bitwarden
WORKDIR /app
COPY bitwarden-audit/ requirements.txt .
RUN apk add --no-cache python3 py3-pip
RUN pip install -r requirements.txt
# We use a numeric ID here to avoid Kubernetes throwing the error: 'container has runAsNonRoot and image has
# non-numeric user (node), cannot verify user is non-root'
USER 1000
